#!/bin/bash

apt-get install build-essential kmod sudo linux-headers-`uname -r` -y
apt-get install -y git curl bash libncurses-dev gawk flex \
        bison openssl libssl-dev dkms libelf-dev libudev-dev \
        libpci-dev libiberty-dev autoconf llvm git clang \
        lld bc

curl https://sh.rustup.rs -sSf | bash -s -- -y
source $HOME/.cargo/env
rustc --version
git clone https://github.com/Rust-for-Linux/linux.git
cd linux
cp .github/workflows/kernel-x86_64-debug.config .config
rustup override set $(scripts/min-tool-version.sh rustc)
rustup component add rust-src
cargo install --locked --version $(scripts/min-tool-version.sh bindgen) bindgen
rustup component add rustfmt
rustup component add clippy
make LLVM=1 olddefconfig
make LLVM=1 -j$(nproc)
