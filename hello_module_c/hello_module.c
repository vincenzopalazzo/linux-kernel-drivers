/**
 * Hello Module for Linux Kernel <VERSION> written in C.
 * */
#include <linux/init.h>
#include <linux/module.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Vincenzo Palazzo <vincenzopalazzodev@gmail.com>");
MODULE_DESCRIPTION("First Hello Module written in C for Linux Kernel");
MODULE_VERSION("0.0.1");

#define EXIT_WITH_SUCCESS 0;

/* Definition of module parametes */
static char *disto_name = "unknown";
module_param(disto_name, charp, S_IRUGO);

/* Call back called to init the module, and the only function of
 * this callback is to use the print kernel function to say hello.
 *
 * Please note that the __init identifier is related to the kernel
 * and it is option.
 *
 * FIXME: Take in consideration when the init function fails to
 * return an error in this way the function will exit will be called
 * to perform the cleanup. */
static int hello_init(void)
{
    printk(KERN_INFO "Hello World\n");
    return EXIT_WITH_SUCCESS;
}

/* Call back called when the module is deinit, and the only function
 * of this callback is to use the print kernel function to say goodbye.
 *
 * Please note that the __exit identifier it is a kernel identifier
 * and it is optional.
 *
 * In addition this identifier can not be used if the callback
 * is manually called inside other procedure like the init procedure. */
static void hello_exit(void)
{
    printk(KERN_INFO "Goodbye!\n");
}

module_init(hello_init);
module_exit(hello_exit);
