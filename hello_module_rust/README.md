# Hello World module in Rust for Linux Kernel

This is an reimplementation of the [hello module writte in C](https://codeberg.org/vincenzopalazzo/linux-kernel-drivers/src/branch/main/hello_module_c/hello_module.c) that use the
template [rust-out-of-tree-module](https://github.com/Rust-for-Linux/rust-out-of-tree-module) to build the module out of the Linux tree!

However, the base functionality for rust are going in the 6.1 kernel version, so you need to build the linux kernel anyway.

The example to compile hello module is the following one.

```sh
$ make KDIR=.../linux-with-rust-support LLVM=1
make -C .../linux-with-rust-support M=$PWD
make[1]: Entering directory '.../linux-with-rust-support'
  RUSTC [M] .../rust-out-of-tree-module/rust_out_of_tree.o
  MODPOST .../rust-out-of-tree-module/Module.symvers
  CC [M]  .../rust-out-of-tree-module/rust_out_of_tree.mod.o
  LD [M]  .../rust-out-of-tree-module/rust_out_of_tree.ko
make[1]: Leaving directory '.../linux-with-rust-support'
```

For details about the Rust support, see https://github.com/Rust-for-Linux/linux.

For details about out-of-tree modules, see https://www.kernel.org/doc/html/latest/kbuild/modules.html.
