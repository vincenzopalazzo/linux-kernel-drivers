// SPDX-License-Identifier: GPL-2.0

//! Hello Module in Rust of Linux Kernel

use kernel::prelude::*;

module! {
    type: HelloModule,
    name: "hello_module",
    author: "Vincenzo Palazzo <vincenzopalazzodev@gmail.com>",
    description: "Hello World module",
    license: "GPL",
}

struct HelloModule;

impl kernel::Module for HelloModule {
    fn init(_name: &'static CStr, _module: &'static ThisModule) -> Result<Self> {
        pr_info!("Hello world\n");
        Ok(HelloModule)
    }
}

impl Drop for HelloModule {
    fn drop(&mut self) {
        pr_info!("Goodbye\n");
    }
}
